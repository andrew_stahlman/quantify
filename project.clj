(defproject quanitfy "0.1.0-SNAPSHOT"
  :description "Personal data capture and visualization in Clojure"
  :url "http://example.com/FIXME"
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.clojure/clojure-contrib "1.2.0"]
                 [org.clojure/clojurescript "0.0-2030"]
                 [com.ashafa/clutch "0.4.0-RC1"]
                 [compojure "1.1.6"]
                 [clj-webdriver "0.6.0"]
                 [com.keminglabs/c2 "0.2.3"]
                 [midje "1.6.0"]
                 [hiccup "1.0.0"]]
  :plugins [[lein-ring "0.8.8"]]
  :ring {:handler quantify.handler/app}
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring-mock "0.1.5"]
                        [lein-midje "3.1.3"]
                        [clojure-csv/clojure-csv "1.3.2"]]}
        :plugins [[lein-midje "3.1.1"]]})
