(ns quantify.db.recs
  (:require [quantify.db.core :as db]))

(def example-rec {
 :date "2013-11-28"
 :records [
           {:label "Work"
            :values {"arrival" "8:30"
                     "departure" "20:00"}}
           {:label "Sleep"
            :values {"bedtime" "23:00"
                     "wakeup" "6:30"}}]})
(def example-rec2 {
 :date "2013-11-27"
 :records [
           {:label "Work"
            :values {"arrival" "9:00"
                     "departure" "23:00"}}
           {:label "Sleep"
            :values {"bedtime" "0:30"
                     "wakeup" "8:30"}}]})

(def example-rec3 {
 :date "2013-11-26"
 :records [
           {:label "Work"
            :values {"arrival" "8:00"
                     "departure" "19:00"}}
           {:label "Sleep"
            :values {"bedtime" "23:30"
                     "wakeup" "6:50"}}]})

(defn bootstrap []
  (map db/store [example-rec example-rec2 example-rec3]))
