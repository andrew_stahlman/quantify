(ns quantify.db.core
  (:require [com.ashafa.clutch :as clutch]))

(use '[com.ashafa.clutch.view-server :only (view-server-exec-string)])

(def recs-db "test-db")
(def design-doc "testing")
(def test-view :some-view)

(defn bootstrap []
  (do
    (clutch/get-database recs-db)
    (clutch/configure-view-server recs-db (view-server-exec-string))))

; Move me
; Ex: 1991-08-08
(defn valid-date? [s]
  (and
    (not (nil? s))
    (not (nil? (re-find #"^\d{4}-\d{2}-\d{2}$" s)))))

; Move me
(defn str->int [s]
  (cond
    (number? s) (int s)
    (and (instance? String s) (not (empty? s))) (str->int (read-string s))
    :else (throw (IllegalArgumentException. "Not a valid numeric string"))))


; Move me
(defn store [rec]
  {:pre [(valid-date? (:date rec))]}
  (clutch/with-db recs-db
    (clutch/put-document rec)))

(defn get-view [key-opts]
  (clutch/get-view recs-db "records" :date_label key-opts))

(defn recs-for-dates [start end]
  (let [data (get-view {:startkey [start] :endkey [end]})]
    (map (fn [rec]
           (let [[date header] (:key rec)
                 values (:value rec)]
             (hash-map :date date
                       :header header
                       :values values))) data)))

(defn filter-header [header recs]
  (filter #(= (:header %1) header) recs))

(defn- make-col-headers [data]
  (into ["date"] (map name
                      (distinct (mapcat keys
                                        (map :values data))))))
(defn- make-rows [data]
  (vec (map (fn [record]
                 (into [(:date record)]
                         (vals (:values record)))) data)))

(defn table [header start end]
  (let [recs (recs-for-dates start end)
        data (filter-header header recs)]
    (into [(make-col-headers data)] (make-rows data))))

(table "Work" "2013-11-27" "2013-11-29")
