(ns quantify.chart.lineplot
  (:use [c2.core :only [unify]])
  (:require [c2.scale :as scale]))

(def width 900)
(def height 300)

(defn x-scale [domain]
  (scale/linear :domain domain :range [0 width]))

(defn y-scale [domain]
  (scale/linear :domain domain :range [0 height]))

(defn mark [x y]
  [:circle {:cx x :cy y :r 3 :fill "blue"}])


(defn gen-rand-data []
  (map-indexed vector (take 15 (repeatedly #(rand-int 100)))))

(defn draw-graph [{:keys [data x-label x-to-num y-label y-to-num]
                   :or {data (gen-rand-data)
                        x-to-num identity y-to-num identity
                        x-label "domain" y-label "range"}}]
  (let [rnge-max (apply max (map second data))
        coords (map-indexed (fn [idx [domain rnge]] (vector domain 
                                 ((x-scale [0 (count data)]) idx)
                                 ((y-scale [0 rnge-max]) rnge))) data)
        line-segs (partition 2 (map rest (interleave (butlast coords) (rest coords))))]
    [:svg {:width width :height height}
     [:g
      [:rect.box {:x 0 :y 0 :width width :height height :fill "white"}]
      (unify line-segs
        (fn [[[x1 y1] [x2 y2]]]
          [:line {:x1 x1 :x2 x2 :y1 y1 :y2 y2 :stroke "red"}]))
      (unify coords 
        (fn [[domain x y]]
          [:g
           [:line {:x1 x :x2 x :y1 0 :y2 height :stroke "gray" :width "1"}]
           [:text {:x x :y (- height 5) :fill "green"} (str domain)]
           (mark x y)]))]]))

(def sample-data
  [["2013/12/11" 2] ["2013/12/12" 1] ["2013/12/13" 3] ["2013/12/14" 0] ["2013/12/15" 3]])
