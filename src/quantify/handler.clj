(ns quantify.handler
  (:use compojure.core)
  (:require [compojure.handler :as handler]
            [compojure.route :as route]))

(defroutes app-routes
  (GET "/" [] "Hello World")
  (GET "/test-table" [] "<table id=\"testtable\"><tbody id=\"table-body\"><tr><td>12-25-2013</td><td>0</td></tr><tr><td>12-26-2013</td><td>1</td></tr></tbody></table>")
  (route/resources "/")
  (route/not-found "Not Found"))

(def app
  (handler/site app-routes))
