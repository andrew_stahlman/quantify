(ns quantify.test.runner
  (:use midje.sweet
        [clj-webdriver.core :only [find-element]])
  (:require [clj-webdriver.taxi :as taxi]))

(fact "Midje tests are being run"
      (+ 1 1) => 2)

(def hello-world-url "http://localhost:3000/")
(def test-table-url "http://localhost:3000/test-table")
(def table-id "#table-body")

(defn start-browser []
    (System/setProperty "webdriver.chrome.driver" "/usr/bin/chromedriver")
    (taxi/set-driver! {:browser :chrome}))

(start-browser)

(defn body-of [url]
  (do
    (taxi/to url)
    (taxi/page-source)))

(defn text-is-present? [txt url]
  (let [body (body-of url)]
    (not (empty? (re-matches (re-pattern (str ".*" txt ".*")) body)))))


(fact "The server is up and running"
      (text-is-present? "Hello World" hello-world-url) => true)

(taxi/to test-table-url)
(fact "We can find an element on the page"
      (taxi/present? "table#testtable") => true)

(def test-table
  [["12-25-2013" 0]
   ["12-26-2013" 1]])

(defn str->num [s]
  (let [n (try
            (read-string s)
            (catch Exception e))]
    (if (number? n) n nil)))

(defn cell-val [cell-text]
  (let [s cell-text
        n (str->num cell-text)]
    (if (nil? n) s n)))

(fact "Cell elements are parsed as numbers as numberic, else as string"
      (cell-val "0") => 0
      (cell-val "3.14") => 3.14
      (cell-val "") => ""
      (cell-val "text") => "text")

(defn cells-to-vec [cell-els]
  (map #(-> (:webelement %) bean :text cell-val) cell-els))

(defn cells-from-row [row-el]
  (cells-to-vec (taxi/find-elements-under row-el {:tag :td})))

(defn find-table-row [table-q row-i]
  (let [rows (map cells-from-row (taxi/find-elements-under table-q {:tag :tr}))]
    (if (< row-i (count rows))
      (nth rows row-i)
      nil)))

(fact "My implementation of find-table-row actually works"
      (find-table-row table-id 0) => ["12-25-2013" 0]
      (find-table-row table-id 1) => ["12-26-2013" 1])


(defn table-as-seq [table-id]
  (let [get-row (partial find-table-row table-id)]
    (loop [r 0 acc []]
      (let [row (get-row r)]
        (if (empty? row)
          acc
          (recur (inc r) (conj acc row)))))))

(defn table-is-displayed? [table table-id url]
  (taxi/to url)
  (= table (table-as-seq table-id)))

(fact "Records are displayed in table form"
      (table-is-displayed? test-table "#table-body" test-table-url) => true)

(taxi/quit)
