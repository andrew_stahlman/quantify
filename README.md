# quantify
Personal analytics in Clojure

## Server
Start the server with

    lein ring server

## Tests
Run tests with 

    lein midje
